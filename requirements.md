### Summary :

The purpose of this code test is to demonstrate your frontend development skills by creating a
responsive web page using an external web service to populate its contents and writing clean,
semantically correct code. Please follow the instructions carefully as you will be tested on both
your technical skills and your attention to detail. Please feel free to use any 3rd party libraries
necessary to complete the task at hand and never hesitate to reach out to Embodee if you have
any questions.
Instructions :

1. Load the data from this API endpoint - https://jsonplaceholder.typicode.com/photos
2. Sort the images on their red color value, from small to big. Take the color value from the
image url - i.e. for "https://via.placeholder.com/600/92c952", the color will be "92c952"
3. Display the images dynamically with JS, using the larger resolution images (“url” field) if the
page is loaded on desktop and the smaller ones (“thumbnailUrl” field) on mobile.
4. Overlay the image title ("title" field) on top of the corresponding image using a contrasting
color (white on darker images, black on lighter ones).
5. Display the first 30 images “on load” and include a “show more” button at the bottom of the
page that will display another 30 images each time it is clicked.
6. Show them in a responsive grid on the webpage - fit maximum number of images in a row
and make them go to the next row when the window is resized.
7. (Optional) add a search field to the top of the page that allows you to filter the image list
based on text contained in the image title.